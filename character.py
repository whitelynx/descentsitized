import pygame

from util import Enum

from actor import Actor


class Character(Actor):
    commands = Enum("LEFT", "RIGHT", "SHOOT", "JUMP")

    def __init__(self, app):
        Actor.__init__(self, app)
        self.inHand = pygame.sprite.GroupSingle()
        self.leftImage = pygame.image.load("images/character.png").convert_alpha()
        self.rightImage = pygame.transform.flip(self.leftImage, True, False)
        self.setImage(self.leftImage)
        self.rect.right = 800

        self.speed = 0.2

        self.keyState = dict()
        for cmd in self.commands.values():
            self.keyState[cmd] = 0

    def update(self, *args):
        self.walkSpeed = 0
        if self.keyState[self.commands.LEFT]:
            self.walkSpeed -= self.speed
        if self.keyState[self.commands.RIGHT]:
            self.walkSpeed += self.speed

        if self.walkSpeed < 0:
            self.setImage(self.leftImage)
        elif self.walkSpeed > 0:
            self.setImage(self.rightImage)

        Actor.update(self, *args)

    def keyEvent(self, command, value):
        if value:
            self.keyDown(command)
        else:
            self.keyUp(command)

    def keyDown(self, command):
        self.keyState[command] += 1

    def keyUp(self, command):
        self.keyState[command] = max(0, self.keyState[command] - 1)
