Descentsitized
==============
An unfinished MiniLD 26 entry (Ludum Dare)

See [the post mortem][] for where I left off. You can also check out [my old Ludum Dare profile][] for _all 3 posts_ I
wrote about this one.

[the post mortem]: https://web.archive.org/web/20141215115355/http://ludumdare.com/compo/2011/05/29/post-mortem-descentsitized/
[my old Ludum Dare profile]: https://web.archive.org/web/20201129082258/http://ludumdare.com/compo/author/whitelynx/


Prerequisites
-------------
You'll need to [install `pipenv`][]. If you already have Python and `pip` installed, the simplest way to install
`pipenv` is:
```sh
pip install --user pipenv
```

[install `pipenv`]: https://pipenv-fork.readthedocs.io/en/latest/install.html#installing-pipenv


Setup
-----
Create a virtual environment and install the needed dependencies using `pipenv`:
```sh
pipenv install
```


Running
-------
Run `descentsitized.py` with `pipenv`:
```sh
pipenv run python descentsitized.py
```
