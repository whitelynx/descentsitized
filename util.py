class Enum(object):
    def __init__(self, *names):
        self.names = names
        self._values = dict()

        nextVal = 0
        for name in names:
            setattr(self, name, nextVal)
            self._values[name] = nextVal
            nextVal += 1

    def __iter__(self):
        return iter(self.names)

    def itervalues(self):
        return iter(self._values.values())

    def values(self):
        return list(self._values.values())

    def iteritems(self):
        return iter(self._values.items())

    def items(self):
        return list(self._values.items())
