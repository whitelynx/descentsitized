import math

import pygame

from actor import Actor


class Zombie(Actor):
    def __init__(self, app):
        Actor.__init__(self, app)
        self.rightImage = pygame.image.load("images/zombie.png").convert_alpha()
        self.leftImage = pygame.transform.flip(self.rightImage, True, False)
        self.setImage(self.rightImage)

        self._walkSpeed = 0.1

    def get_walkSpeed(self):
        return self._walkSpeed

    def set_walkSpeed(self, value):
        self._walkSpeed = value
        if value > 0:
            self.setImage(self.rightImage)
        elif value < 0:
            self.setImage(self.leftImage)

    walkSpeed = property(get_walkSpeed, set_walkSpeed)

    def update(self, *args):
        if self.rect.right >= self.app.screen.get_width():
            print(self.rect.right)
            print("Reverse! (from right)")
            self.walkSpeed = -math.fabs(self.walkSpeed)

        elif self.rect.left <= 0:
            print(self.rect.left)
            print("Reverse! (from left)")
            self.walkSpeed = math.fabs(self.walkSpeed)

        Actor.update(self, *args)
