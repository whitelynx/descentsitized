#!/usr/bin/env python
import sys
import functools
import logging
from random import randint

import pygame

from util import Enum
from character import Character
from zombie import Zombie


logging.basicConfig()
logger = logging.getLogger("descentsitized")


class Application(object):
    def __init__(self):
        self.screenWidth = 800
        self.screenHeight = 600

        self.black = 0, 0, 0
        self.darkgrey = 10, 10, 10

        # Set up pygame objects
        pygame.init()
        self.screen = pygame.display.set_mode((self.screenWidth, self.screenHeight))
        self.clock = pygame.time.Clock()

        # Global keybindings
        self.keys = {
            pygame.K_ESCAPE: self.shutdown,
        }

        # Sprite rendering
        self.layers = Enum(
            "world",
            "character",
            "enemies",
            "items",
        )
        self.world = pygame.sprite.Group()
        self.entities = pygame.sprite.Group()
        self.sprites = pygame.sprite.LayeredUpdates()

        self.createEntities()

    def createCharacter(self):
        char = Character(self)
        self.entities.add(char)
        self.sprites.add(char, layer=self.layers.character)

        # Set up character control keys.
        self.keys[pygame.K_LEFT] = functools.partial(char.keyEvent, char.commands.LEFT)
        self.keys[pygame.K_RIGHT] = functools.partial(
            char.keyEvent, char.commands.RIGHT
        )

    def createEntities(self):
        for i in range(randint(5, 15)):
            zombie = Zombie(self)
            zombie.rect.right = randint(zombie.rect.width, self.screenWidth)
            zombie.rect.bottom = randint(zombie.rect.height, self.screenHeight)
            self.entities.add(zombie)
            self.sprites.add(zombie, layer=self.layers.enemies)

    def shutdown(self, value):
        self.screen.fill(self.black)
        pygame.display.flip()
        sys.exit()

    def runloop(self):
        while 1:
            self.clock.tick(60)

            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    sys.exit()
                elif event.type == pygame.KEYDOWN:
                    if event.key in self.keys:
                        try:
                            self.keys[event.key](True)
                        except Exception as e:
                            logger.error("Exception in key handler! %r", e)
                elif event.type == pygame.KEYUP:
                    if event.key in self.keys:
                        try:
                            self.keys[event.key](False)
                        except Exception as e:
                            logger.error("Exception in key handler! %r", e)

            self.sprites.update()

            self.screen.fill(self.darkgrey)
            dirty_rects = self.sprites.draw(self.screen)
            pygame.display.update(dirty_rects)


app = Application()
app.runloop()
