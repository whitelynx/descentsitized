import pygame


class Actor(pygame.sprite.Sprite):
    # Average human male height in Finland is 1.784m
    # (Why Finland? Because I felt like it.)
    # Our human is 28 pixels high
    # Therefore, 0.06371428571428571 meters per pixel.
    # Gravity is 9.8 m/s**2
    # Therefore, 153.81165919282515 px/s**2
    # But this in in .1px / ms...
    gravity = 0.15381165919282515

    # Terminal velocity is about 90 m/s for minimal wind resitance.
    # Therefore, 1412.5560538116592 px/s
    # Since this is in .1px / ms...
    terminalVelocity = 1.4125560538116592

    # Movement works on a scale of pixels / movementScale
    movementScale = 10

    def __init__(self, app):
        pygame.sprite.Sprite.__init__(self)
        self.app = app
        self.walkSpeed = 0
        self.verticalSpeed = 0
        self.jumpRequiresGround = True  # set to False for jetpack-like stuff
        self.rect = pygame.rect.Rect(0, 0, 8, 28)
        self.bodyRect = pygame.rect.Rect(0, 0, 50, 230)
        self.feetRect = pygame.rect.Rect(30, 230, 50, 50)

    def get_rect(self):
        return self._rect

    def set_rect(self, value):
        self._rect = value
        self._movementRect = pygame.rect.Rect(
            self.rect.left * self.movementScale,
            self.rect.top * self.movementScale,
            self.rect.width * self.movementScale,
            self.rect.height * self.movementScale,
        )

    def get_movementRect(self):
        return self._movementRect

    def set_movementRect(self, value):
        self._movementRect = value
        self._rect = pygame.rect.Rect(
            self.rect.left / self.movementScale,
            self.rect.top / self.movementScale,
            self.rect.width / self.movementScale,
            self.rect.height / self.movementScale,
        )

    movementRect = property(get_movementRect, set_movementRect)

    def setImage(self, image):
        topleft = self.rect.topleft
        self.image = image
        self.rect = self.image.get_rect()
        self.rect.topleft = topleft

    def onGround(self):
        footRect = self.rect.copy()
        footRect.height = 1
        footRect.bottom = self.rect.bottom

        collisions = pygame.sprite.spritecollide(
            self,
            self.app.world,
            False,
            collided=lambda x, y: footRect.colliderect(y.rect),
        )
        return collisions

    def jump(self, speed):
        if self.jumpRequiresGround:
            if not self.onGround():
                return

        self.walkSpeed = speed
        screenWidth, screenHeight = 800, 600

    def update(self, *args):
        self.verticalMovement = self.verticalSpeed * self.app.clock.get_time()
        self.verticalSpeed += self.gravity * self.app.clock.get_time()
        if self.verticalSpeed > self.terminalVelocity:
            self.verticalSpeed = self.terminalVelocity

        self.rect.move_ip(
            (self.walkSpeed * self.app.clock.get_time(), self.verticalMovement)
        )

        if self.verticalSpeed > 0:
            if self.onGround():
                print("On ground!")
                self.verticalSpeed = 0

        else:
            if self.hitCeiling():
                print("Hit ceiling!")
                self.verticalSpeed = 0

        if self.rect.right > self.app.screen.get_width():
            print(self.rect.right)
            self.rect.right = self.app.screen.get_width()
            print("Clamped to screen right.", self.walkSpeed, self.rect.right)
        elif self.rect.left < 0:
            print(self.rect.left)
            self.rect.left = 0
            print("Clamped to screen left.", self.walkSpeed, self.rect.left)

        if self.rect.bottom > self.app.screen.get_height():
            self.rect.bottom = self.app.screen.get_height()
            self.verticalSpeed = 0
        elif self.rect.top < 0:
            self.rect.top = 0
            self.verticalSpeed = 0
